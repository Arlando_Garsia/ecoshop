<!--======= FOOTER =========-->
<footer>
    <div class="container">

        <!-- ABOUT Location -->
        <div class="col-md-3">
            <div class="about-footer"> <img class="margin-bottom-30" src="images/logo-foot.png" alt="" >
                <p><i class="icon-pointer"></i>г. Барнаул, Ленина 46<br>
                    Россия.</p>
                <p><i class="icon-call-end"></i>8-952-000-5161</p>
                <p><i class="icon-envelope"></i>altair.996@yandex.ru</p>
            </div>
        </div>

        <!-- HELPFUL LINKS -->
        <div class="col-md-3 col-md-offset-1">
            <h6>ПОЛЕЗНЫЕ ССЫЛКИ</h6>
            <ul class="link">
                <li><a href="shop.php">Каталог</a></li>
                <li><a href="">Постоянная Рубрика</a></li>
                <li><a href="">Политика Конфиденциальности</a></li>
                <li><a href="blog.php">Новости</a></li>
            </ul>
        </div>

        <!-- SHOP -->
        <div class="col-md-3 col-md-offset-1">
            <h6>Магазин</h6>
            <ul class="link">
                <li><a href="about.php">О Нас</a></li>
                <li><a href="">Доставка</a></li>
                <li><a href="contact.php">Обратная Связь</a></li>
                <li><a href="">Поддержка</a></li>
            </ul>
        </div>

        <!-- Rights -->

        <div class="rights">
            <p>©  2019 Киселев Игорь Сергеевич</p>
            <div class="scroll"> <a href="#wrap" class="go-up"><i class="lnr lnr-arrow-up"></i></a> </div>
        </div>
    </div>
</footer>
