<section class="news-letter padding-top-150 padding-bottom-150">
    <div class="container">
        <div class="heading light-head text-center margin-bottom-30">
            <h4>Подпишитесь на рассылку</h4>
            <span>Предлагаем вам подписаться на бесплатную новостную рассылку, чтобы вовремя получать новости</span> </div>
        <form>
            <input type="email" placeholder="E-mail" required>
            <button type="submit">Подписаться</button>
        </form>
    </div>
</section>