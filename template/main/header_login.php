<!-- header -->
<header>
    <div class="sticky">
        <div class="container">

            <!-- Logo -->
            <div class="logo"> <a href="\"><img class="img-responsive" src="../../images/logo.png" alt="" ></a> </div>
            <nav class="navbar ownmenu">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav-open-btn" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"><i class="fa fa-navicon"></i></span> </button>
                </div>

                <!-- NAV -->
                <div class="collapse navbar-collapse" id="nav-open-btn">
                    <ul class="nav">

                        <!-- Link Option -->
                        <li class="dropdown active"> <a href="../../shop.php" class="dropdown-toggle">Книги</a>
                            <div class="dropdown-menu">
                                <ul class="col-sm-6">
                                    <h6>Жанры</h6>
                                    <li> <a href="../../shop.php">Фантастика</a></li>
                                    <li> <a href="shop_02.php"> sarees</a></li>
                                    <li> <a href="shop_02.php"> kurtas</a></li>
                                    <li> <a href="shop_02.php"> shorts</a></li>
                                    <li> <a href="shop_02.php"> winter</a></li>
                                    <li> <a href="shop_02.php"> jeans</a></li>
                                    <li> <a href="shop_02.php"> bra</a></li>
                                    <li> <a href="shop_02.php"> babydools</a> </li>
                                </ul>
                            </div>
                        </li>

                        <!-- Two Link Option -->
                        <li class="dropdown"> <a href="../../shop.php" class="dropdown-toggle">Канцтовары</a>
                            <div class="dropdown-menu">
                                <ul class="col-sm-6">
                                    <h6>Разделы</h6>
                                    <li> <a href="../../shop.php">Пеналы</a></li>
                                    <li> <a href="shop_02.html"> sarees</a></li>
                                    <li> <a href="shop_02.html"> kurtas</a></li>
                                    <li> <a href="shop_02.html"> shorts</a></li>
                                    <li> <a href="shop_02.html"> winter</a></li>
                                    <li> <a href="shop_02.html"> jeans</a></li>
                                    <li> <a href="shop_02.html"> bra</a></li>
                                    <li> <a href="shop_02.html"> babydools</a> </li>
                                </ul>
                            </div>
                        </li>

                        <li> <a href="../../blog.php">Новости</a> </li>

                        <li> <a href="../../about.php">О Нас</a> </li>

                        <li> <a href="../../contact.php">Обратная связь</a> </li>
                    </ul>
                </div>

                <!-- Nav Right -->
                <div class="nav-right">
                    <ul class="navbar-right">

                        <!-- USER INFO -->
                        <li class="dropdown user-acc"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" ><i class="icon-user"></i> </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <h6>Гость</h6>
                                </li>
                                <li><a href="../../login.php">Войти</a></li>
                                <li><a href="../../register.php">Зарегистрироваться</a></li>
                            </ul>
                        </li>

                        <!-- USER BASKET -->
                        <li class="dropdown user-basket"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"><i class="icon-basket-loaded"></i> </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <div class="media-left">
                                        <div class="cart-img"> <a href="../../product.php"> <img class="media-object img-responsive" src="../../images/cart-img-1.jpg" alt="..."> </a> </div>
                                    </div>
                                    <div class="media-body">
                                        <h6 class="media-heading">WOOD CHAIR</h6>
                                        <span class="price">129.00 RUB</span> <span class="qty">Кол-во: 01</span> </div>
                                </li>
                                <li>
                                    <div class="media-left">
                                        <div class="cart-img"> <a href="../../product.php"> <img class="media-object img-responsive" src="../../images/cart-img-2.jpg" alt="..."> </a> </div>
                                    </div>
                                    <div class="media-body">
                                        <h6 class="media-heading">WOOD STOOL</h6>
                                        <span class="price">129.00 RUB</span> <span class="qty">Кол-во: 01</span> </div>
                                </li>
                                <li>
                                    <h5 class="text-center">Итого: 258.00 RUB</h5>
                                </li>
                                <li class="margin-0">
                                    <div class="row">
                                        <div class="col-xs-6"> <a href="../../cart.php" class="btn">Корзина</a></div>
                                        <div class="col-xs-6 "> <a href="../../checkout.php" class="btn">Оплатить</a></div>
                                    </div>
                                </li>
                            </ul>
                        </li>

                        <!-- SEARCH BAR -->
                        <li class="dropdown"> <a href="javascript:void(0);" class="search-open"><i class=" icon-magnifier"></i></a>
                            <div class="search-inside animated bounceInUp"> <i class="icon-close search-close"></i>
                                <div class="search-overlay"></div>
                                <div class="position-center-center">
                                    <div class="search">
                                        <form>
                                            <input type="search" placeholder="Поиск">
                                            <button type="submit"><i class="icon-check"></i></button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</header>

