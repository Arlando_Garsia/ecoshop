<?php
require_once('config/db.php');
$db = db::getConnection();

/** Получение категорий Книги */
$sql = ("SELECT * FROM category WHERE parent_id = 0");
$result = $db->prepare($sql);
$result->execute();
$book = $result->fetchAll(PDO::FETCH_ASSOC);

/** Получение категорий Канцтовары */
$sql = ("SELECT * FROM category WHERE parent_id = 1");
$result = $db->prepare($sql);
$result->execute();
$stationery = $result->fetchAll(PDO::FETCH_ASSOC);
