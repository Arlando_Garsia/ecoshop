<?php

class db {

    public static function getConnection()
    {
        try {
            $db = new PDO("mysql:host=localhost;dbname=ecoshop;port=3306;","root", "");
        } catch (PDOException $exception) {
            die($exception->getMessage());
        }

        $db->exec("set names utf8");

        return $db;
    }
}
