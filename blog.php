<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="M_Adnan">
    <title>ECOSHOP - Multipurpose eCommerce HTML5 Template</title>

    <!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="rs-plugin/css/settings.css" media="screen" />

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="css/ionicons.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    <!-- JavaScripts -->
    <script src="js/modernizr.js"></script>

    <!-- Online Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Playfair+Display:400,700,900' rel='stylesheet' type='text/css'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>

<!-- LOADER -->
<div id="loader">
    <div class="position-center-center">
        <div class="ldr"></div>
    </div>
</div>

<!-- Wrap -->
<div id="wrap">

    <!- Подключение шапки сайта ->
    <?php require_once('template/main/header.php') ?>

    <!--======= SUB BANNER =========-->
    <section class="sub-bnr" data-stellar-background-ratio="0.5">
        <div class="position-center-center">
            <div class="container">
                <h4>Новости</h4>
                <ol class="breadcrumb">
                    <li><a href="/">Главная</a></li>
                    <li class="active">Новости</li>
                </ol>
            </div>
        </div>
    </section>

    <!-- Content -->
    <div id="content">

        <!-- Blog List -->
        <section class="blog-list padding-top-100 padding-bottom-100">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">

                        <!-- Article -->
                        <article>
                            <!-- Post Img -->
                            <img class="img-responsive" src="images/blog-list-img-1.jpg" alt="" >
                            <!-- Tittle -->
                            <div class="post-tittle left"> <a href="#." class="tittle">The unique Chair By ecoshop</a>
                                <!-- Post Info -->
                                <span><i class="primary-color icon-user"></i> by admin</span> <span><i class="primary-color icon-calendar"></i> April 27, 2016</span> <span><i class="primary-color icon-bubble"></i> 05</span> <span><i class="primary-color icon-tag"></i> Furniture</span> </div>
                            <!-- Post Content -->
                            <div class="text-left">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam volutpat dui at lacus aliquet, a consequat enim aliquet. Integer molestie sit amet sem et faucibus. Nunc ornare pharetra dui, vitae auctor orci fringilla eget. Pellentesque in placerat felis. Etiam mollis venenatis luctus. Morbi ac scelerisque mauris. Etiam sodales a nulla ornare viverra. Nunc at blandit neque, bibendum varius purus. <br>
                                    <br>
                                    Nam sit amet sapien vitae enim vehicula tincidunt. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc faucibus imperdiet vulputate. Morbi volutpat leo iaculis elit vehicula, eu convallis magna finibus. Suspendisse tristique ullamcorper erat a elementum. Cras eget elit non nunc aliquam ullamcorper quis sed metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec malesuada, erat in ullamcorper bibendum, elit lacus mattis lorem, quis luctus diam lorem vel ligula.</p>
                                <a href="blog_detail.php" class="btn">READ MORE</a> </div>
                        </article>

                        <!-- Article -->
                        <article>
                            <!-- Post Img -->
                            <img class="img-responsive" src="images/blog-list-img-2.jpg" alt="" >
                            <!-- Tittle -->
                            <div class="post-tittle left"> <a href="#." class="tittle">Look Beautiful in this Seasons</a>
                                <!-- Post Info -->
                                <span><i class="primary-color icon-user"></i> by admin</span> <span><i class="primary-color icon-calendar"></i> April 27, 2016</span> <span><i class="primary-color icon-bubble"></i> 05</span> <span><i class="primary-color icon-tag"></i> Fashion</span> </div>
                            <!-- Post Content -->
                            <div class="text-left">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam volutpat dui at lacus aliquet, a consequat enim aliquet. Integer molestie sit amet sem et faucibus. Nunc ornare pharetra dui, vitae auctor orci fringilla eget. Pellentesque in placerat felis. Etiam mollis venenatis luctus. Morbi ac scelerisque mauris. Etiam sodales a nulla ornare viverra. Nunc at blandit neque, bibendum varius purus. <br>
                                    <br>
                                    Nam sit amet sapien vitae enim vehicula tincidunt. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc faucibus imperdiet vulputate. Morbi volutpat leo iaculis elit vehicula, eu convallis magna finibus. Suspendisse tristique ullamcorper erat a elementum. Cras eget elit non nunc aliquam ullamcorper quis sed metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec malesuada, erat in ullamcorper bibendum, elit lacus mattis lorem, quis luctus diam lorem vel ligula.</p>
                                <a href="#." class="btn">READ MORE</a> </div>
                        </article>

                        <!-- Article -->
                        <article>
                            <!-- Post Img -->
                            <img class="img-responsive" src="images/blog-list-img-3.jpg" alt="" >
                            <!-- Tittle -->
                            <div class="post-tittle left"> <a href="#." class="tittle">We Craft An Awesome for your Home</a>
                                <!-- Post Info -->
                                <span><i class="primary-color icon-user"></i> by admin</span> <span><i class="primary-color icon-calendar"></i> April 27, 2016</span> <span><i class="primary-color icon-bubble"></i> 05</span> <span><i class="primary-color icon-tag"></i> Wood</span> </div>
                            <!-- Post Content -->
                            <div class="text-left">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam volutpat dui at lacus aliquet, a consequat enim aliquet. Integer molestie sit amet sem et faucibus. Nunc ornare pharetra dui, vitae auctor orci fringilla eget. Pellentesque in placerat felis. Etiam mollis venenatis luctus. Morbi ac scelerisque mauris. Etiam sodales a nulla ornare viverra. Nunc at blandit neque, bibendum varius purus. <br>
                                    <br>
                                    Nam sit amet sapien vitae enim vehicula tincidunt. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc faucibus imperdiet vulputate. Morbi volutpat leo iaculis elit vehicula, eu convallis magna finibus. Suspendisse tristique ullamcorper erat a elementum. Cras eget elit non nunc aliquam ullamcorper quis sed metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec malesuada, erat in ullamcorper bibendum, elit lacus mattis lorem, quis luctus diam lorem vel ligula.</p>
                                <a href="#." class="btn">READ MORE</a> </div>
                        </article>

                        <!-- Article -->
                        <article>
                            <!-- Post Img -->
                            <img class="img-responsive" src="images/blog-list-img-4.jpg" alt="" >
                            <!-- Tittle -->
                            <div class="post-tittle left"> <a href="#." class="tittle">The Classic Razor for the Modern Man</a>
                                <!-- Post Info -->
                                <span><i class="primary-color icon-user"></i> by admin</span> <span><i class="primary-color icon-calendar"></i> April 27, 2016</span> <span><i class="primary-color icon-bubble"></i> 05</span> <span><i class="primary-color icon-tag"></i> Personal</span> </div>
                            <!-- Post Content -->
                            <div class="text-left">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam volutpat dui at lacus aliquet, a consequat enim aliquet. Integer molestie sit amet sem et faucibus. Nunc ornare pharetra dui, vitae auctor orci fringilla eget. Pellentesque in placerat felis. Etiam mollis venenatis luctus. Morbi ac scelerisque mauris. Etiam sodales a nulla ornare viverra. Nunc at blandit neque, bibendum varius purus. <br>
                                    <br>
                                    Nam sit amet sapien vitae enim vehicula tincidunt. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc faucibus imperdiet vulputate. Morbi volutpat leo iaculis elit vehicula, eu convallis magna finibus. Suspendisse tristique ullamcorper erat a elementum. Cras eget elit non nunc aliquam ullamcorper quis sed metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec malesuada, erat in ullamcorper bibendum, elit lacus mattis lorem, quis luctus diam lorem vel ligula.</p>
                                <a href="#." class="btn">READ MORE</a> </div>
                        </article>

                        <!-- Pagination -->
                        <ul class="pagination in-center">
                            <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                        </ul>
                    </div>

                    <!-- Sider Bar -->
                    <div class="col-md-3">

                        <!-- SEARCH -->
                        <div class="search">
                            <input class="form-control" type="search" placeholder="Search">
                            <button type="submit"><i class="fa fa-search"></i></button>
                        </div>
                        <div class="shop-sidebar">

                            <!-- Category -->
                            <h5 class="shop-tittle margin-bottom-30">category</h5>
                            <ul class="shop-cate">
                                <li><a href="#."> Chair <span>24</span></a></li>
                                <li><a href="#."> Bag <span>122</span></a></li>
                                <li><a href="#."> Soffa <span>09</span></a></li>
                                <li><a href="#."> Bed <span>12</span></a></li>
                                <li><a href="#."> Shoes <span>98</span></a></li>
                                <li><a href="#."> Table <span>34</span></a></li>
                                <li><a href="#."> Bedsheets <span>23</span></a></li>
                                <li><a href="#."> Curtains <span>43</span></a></li>
                                <li><a href="#."> TV Cabinets <span>12</span></a></li>
                                <li><a href="#."> Clocks <span>18</span></a></li>
                                <li><a href="#."> Towels <span>25</span></a></li>
                            </ul>

                            <!-- Recent Post -->
                            <h5 class="shop-tittle margin-top-60 margin-bottom-30">recent post</h5>
                            <ul class="papu-post margin-top-20">
                                <li class="media">
                                    <div class="media-left"> <a href="#"> <img class="media-object" src="images/sm-post-1.jpg" alt=""></a> </div>
                                    <div class="media-body"> <a class="media-heading" href="#.">Nullam volutpat dui at
                                            a consequat enimiquet</a> <span>Posted on Sep 27</span> </div>
                                </li>
                                <li class="media">
                                    <div class="media-left"> <a href="#"> <img class="media-object" src="images/sm-post-2.jpg" alt=""></a> </div>
                                    <div class="media-body"> <a class="media-heading" href="#.">Nullam volutpat dui at
                                            a consequat enimiquet</a> <span>Posted on Sep 27</span> </div>
                                </li>
                                <li class="media">
                                    <div class="media-left"> <a href="#"> <img class="media-object" src="images/sm-post-3.jpg" alt=""></a> </div>
                                    <div class="media-body"> <a class="media-heading" href="#.">Nullam volutpat dui at
                                            a consequat enimiquet</a> <span>Posted on Sep 27</span> </div>
                                </li>
                            </ul>

                            <!-- TAGS -->
                            <h5 class="shop-tittle margin-top-60 margin-bottom-30">PAUPLAR TAGS</h5>
                            <ul class="shop-tags">
                                <li><a href="#.">Towels</a></li>
                                <li><a href="#.">Chair</a></li>
                                <li><a href="#.">Bedsheets</a></li>
                                <li><a href="#.">Shoe</a></li>
                                <li><a href="#.">Curtains</a></li>
                                <li><a href="#.">Clocks</a></li>
                                <li><a href="#.">TV Cabinets</a></li>
                                <li><a href="#.">Best Seller</a></li>
                                <li><a href="#.">Top Selling</a></li>
                            </ul>

                            <!-- BRAND -->
                            <h5 class="shop-tittle margin-top-60 margin-bottom-30">archives</h5>
                            <ul class="shop-cate">
                                <li><a href="#."> January 2015 </a></li>
                                <li><a href="#."> February 2015 </a></li>
                                <li><a href="#."> March 2015 </a></li>
                                <li><a href="#."> April 2015 </a></li>
                                <li><a href="#."> May 2015 </a></li>
                            </ul>

                            <!-- SIDE BACR BANER -->
                            <div class="side-bnr margin-top-50"> <img class="img-responsive" src="images/sidebar-bnr.jpg" alt="">
                                <div class="position-center-center"> <span class="price"><small>$</small>299</span>
                                    <div class="bnr-text">look
                                        hot
                                        with
                                        style</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!- Подключение новостной рассылки сайта ->
        <?php require_once('template/main/newsletter.php') ?>
    </div>

    <!- Подключение подвала сайта ->
    <?php require_once('template/main/footer.php') ?>
</div>
<script src="js/jquery-1.11.3.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/own-menu.js"></script>
<script src="js/jquery.lighter.js"></script>
<script src="js/owl.carousel.min.js"></script>

<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
<script type="text/javascript" src="rs-plugin/js/jquery.tp.t.min.js"></script>
<script type="text/javascript" src="rs-plugin/js/jquery.tp.min.js"></script>
<script src="js/main.js"></script>
</body>
</html>