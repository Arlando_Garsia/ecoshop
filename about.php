<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="M_Adnan">
    <title>ECOSHOP - Multipurpose eCommerce HTML5 Template</title>

    <!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="rs-plugin/css/settings.css" media="screen" />

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="css/ionicons.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    <!-- JavaScripts -->
    <script src="js/modernizr.js"></script>

    <!-- Online Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Playfair+Display:400,700,900' rel='stylesheet' type='text/css'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>

<!-- LOADER -->
<div id="loader">
    <div class="position-center-center">
        <div class="ldr"></div>
    </div>
</div>

<!-- Wrap -->
<div id="wrap">

    <!- Подключение шапки сайта ->
    <?php require_once('template/main/header.php') ?>

    <!--======= SUB BANNER =========-->
    <section class="sub-bnr" data-stellar-background-ratio="0.5">
        <div class="position-center-center">
            <div class="container">
                <h4>О Нас</h4>
                <ol class="breadcrumb">
                    <li><a href="/">Главная</a></li>
                    <li class="active">О Нас</li>
                </ol>
            </div>
        </div>
    </section>

    <!-- Content -->
    <div id="content">

        <!-- History -->
        <section class="history-block padding-top-100 padding-bottom-100">
            <div class="container">
                <div class="row">
                    <div class="col-xs-10 center-block">
                        <div class="col-sm-9 center-block">
                            <h4>A Brief History</h4>
                            <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam volutpat dui at lacus aliquet, a consequat enim aliquet. Integer molestie sit amet sem et faucibus. Nunc ornare pharetra dui, vitae auctor orci fringilla eget. Pellentesque in placerat felis. Etiam mollis venenatis luctus. Morbi ac scelerisque mauris. Etiam sodales a nulla ornare viverra. Nunc at blandit neque, bibendum varius purus. <br>
                                <br>
                                Nam sit amet sapien vitae enim vehicula tincidunt. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc faucibus imperdiet vulputate. Morbi volutpat leo iaculis elit vehicula, eu convallis magna finibus. Suspendisse tristique ullamcorper erat a elementum. Cras eget elit non nunc aliquam ullamcorper quis sed metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec malesuada, erat in ullamcorper bibendum, elit lacus mattis lorem, quis luctus diam lorem vel ligula. </p>
                        </div>

                        <!-- IMG -->
                        <img class="img-responsive margin-top-80 margin-bottom-80" src="images/about-img.jpg" alt="">
                        <div class="vision-text">
                            <div class="col-lg-5">
                                <h5 class="text-left">our vision</h5>
                                <h2>We craft awesome stuff with great experiences</h2>
                            </div>
                            <div class="col-lg-7">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam volutpat dui at lacus aliquet, a consequat enim aliquet. Integer molestie sit amet sem et faucibus. Nunc ornare pharetra dui, vitae auctor orci fringilla eget. Pellentesque in placerat felis. Etiam mollis venenatis luctus. <br>
                                    <br>
                                    Morbi ac scelerisque mauris. Etiam sodales a nulla ornare viverra. Nunc at blandit neque, bibendum varius purus. Nam sit amet sapien vitae enim vehicula tincidunt. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc faucibus imperdiet vulputate. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- OUR TEAM -->
        <section class="our-team light-gray-bg padding-top-150 padding-bottom-100">
            <div class="container">
                <div class="heading text-center">
                    <h4>OUR TEAM</h4>
                    <span>United by love & help to build great brands</span> </div>

                <!-- TEAM -->
                <ul class="row">

                    <!-- Member -->
                    <li class="col-md-4 text-center animate fadeInUp" data-wow-delay="0.4s">
                        <article>
                            <!-- abatar -->
                            <div class="avatar"> <img class="img-circle" src="images/team-1.jpg" alt="" >
                                <!-- Team hover -->
                                <div class="team-hover">
                                    <div class="position-center-center">
                                        <div class="social-icons"> <a href="#."><i class="icon-social-youtube"></i></a> <a href="#."><i class="icon-social-twitter"></i></a> <a href="#."><i class="icon-social-dribbble"></i></a> </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Team Detail -->
                            <div class="team-names">
                                <h6>JOSEPH MARK</h6>
                                <p>CEO & FOUNDER</p>
                            </div>
                        </article>
                    </li>

                    <!-- Member -->
                    <li class="col-md-4 text-center animate fadeInUp" data-wow-delay="0.6s">
                        <article>
                            <!-- abatar -->
                            <div class="avatar"> <img class="img-circle" src="images/team-2.jpg" alt="" >
                                <!-- Team hover -->
                                <div class="team-hover">
                                    <div class="position-center-center">
                                        <div class="social-icons"> <a href="#."><i class="icon-social-facebook"></i></a> <a href="#."><i class="icon-social-twitter"></i></a> <a href="#."><i class="icon-social-dribbble"></i></a> </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Team Detail -->
                            <div class="team-names">
                                <h6>jennifer rod</h6>
                                <p>DESIGNER</p>
                            </div>
                        </article>
                    </li>

                    <!-- Member -->
                    <li class="col-md-4 text-center animate fadeInUp" data-wow-delay="0.8s">
                        <article>
                            <!-- abatar -->
                            <div class="avatar"> <img class="img-circle" src="images/team-3.jpg" alt="" >
                                <!-- Team hover -->
                                <div class="team-hover">
                                    <div class="position-center-center">
                                        <div class="social-icons"> <a href="#."><i class="icon-social-facebook"></i></a> <a href="#."><i class="icon-social-twitter"></i></a> <a href="#."><i class="icon-social-dribbble"></i></a> </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Team Detail -->
                            <div class="team-names">
                                <h6>natasha singh</h6>
                                <p>DEVELOPER</p>
                            </div>
                        </article>
                    </li>

                    <!-- Member -->
                    <li class="col-md-4 text-center">
                        <article>
                            <!-- abatar -->
                            <div class="avatar"> <img class="img-circle" src="images/team-4.jpg" alt="" >
                                <!-- Team hover -->
                                <div class="team-hover">
                                    <div class="position-center-center">
                                        <div class="social-icons"> <a href="#."><i class="icon-social-facebook"></i></a> <a href="#."><i class="icon-social-twitter"></i></a> <a href="#."><i class="icon-social-dribbble"></i></a> </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Team Detail -->
                            <div class="team-names">
                                <h6>JOSEPH MARK</h6>
                                <p>Product Designer</p>
                            </div>
                        </article>
                    </li>

                    <!-- Member -->
                    <li class="col-md-4 text-center">
                        <article>
                            <!-- abatar -->
                            <div class="avatar"> <img class="img-circle" src="images/team-5.jpg" alt="" >
                                <!-- Team hover -->
                                <div class="team-hover">
                                    <div class="position-center-center">
                                        <div class="social-icons"> <a href="#."><i class="icon-social-facebook"></i></a> <a href="#."><i class="icon-social-twitter"></i></a> <a href="#."><i class="icon-social-dribbble"></i></a> </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Team Detail -->
                            <div class="team-names">
                                <h6>jennifer rod</h6>
                                <p>Quality Head</p>
                            </div>
                        </article>
                    </li>

                    <!-- Member -->
                    <li class="col-md-4 text-center">
                        <article>
                            <!-- abatar -->
                            <div class="avatar"> <img class="img-circle" src="images/team-6.jpg" alt="" >
                                <!-- Team hover -->
                                <div class="team-hover">
                                    <div class="position-center-center">
                                        <div class="social-icons"> <a href="#."><i class="icon-social-facebook"></i></a> <a href="#."><i class="icon-social-twitter"></i></a> <a href="#."><i class="icon-social-dribbble"></i></a> </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Team Detail -->
                            <div class="team-names">
                                <h6>natasha singh</h6>
                                <p>DEVELOPER</p>
                            </div>
                        </article>
                    </li>
                </ul>
            </div>
        </section>

        <!- Подключение новостной рассылки сайта ->
        <?php require_once('template/main/newsletter.php') ?>
    </div>

    <!- Подключение подвала сайта ->
    <?php require_once('template/main/footer.php') ?>
</div>
<script src="js/jquery-1.11.3.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/own-menu.js"></script>
<script src="js/jquery.lighter.js"></script>
<script src="js/owl.carousel.min.js"></script>

<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
<script type="text/javascript" src="rs-plugin/js/jquery.tp.t.min.js"></script>
<script type="text/javascript" src="rs-plugin/js/jquery.tp.min.js"></script>
<script src="js/main.js"></script>

</body>
</html>
