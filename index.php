<?php
//$root = $_SERVER['DOCUMENT_ROOT'];

session_start();
require_once('config/db.php');
$db = db::getConnection();

/** Получение товара для блока "Новинки литературы" */
//$sql = ("SELECT * FROM product WHERE is_stock = 1 AND is_new_arrival = 1 LIMIT 4");
$sql = ("SELECT * FROM product WHERE is_stock = 1 AND is_new_arrival = 1 ORDER BY RAND() LIMIT 8");
$result = $db->prepare($sql);
$result->execute();
$new_book = $result->fetchAll(PDO::FETCH_ASSOC);

/** Получение товара для блока "Горящие товары" */
$sql = ("SELECT * FROM product WHERE is_stock = 1 AND discount > 9 AND is_new_arrival != 1 ORDER BY RAND() LIMIT 8");
$result = $db->prepare($sql);
$result->execute();
$burn_book = $result->fetchAll(PDO::FETCH_ASSOC);

/** Получение товара для блока "Популярное" */
$sql = ("SELECT * FROM product WHERE is_stock = 1 AND rating > 3 ORDER BY RAND() LIMIT 8");
$result = $db->prepare($sql);
$result->execute();
$best_book = $result->fetchAll(PDO::FETCH_ASSOC);

/** Получение товара для блока "Скоро в продаже" */
$sql = ("SELECT * FROM product WHERE is_stock = 0 AND rating = 5 ORDER BY RAND() LIMIT 8");
$result = $db->prepare($sql);
$result->execute();
$soon_book = $result->fetchAll(PDO::FETCH_ASSOC);

?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="M_Adnan">
    <title>ECOSHOP - Multipurpose eCommerce HTML5 Template</title>

    <!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="rs-plugin/css/settings.css" media="screen" />

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="css/ionicons.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    <!-- JavaScripts -->
    <script src="js/modernizr.js"></script>

    <!-- Online Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Playfair+Display:400,700,900' rel='stylesheet' type='text/css'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>

<!-- LOADER -->
<div id="loader">
    <div class="position-center-center">
        <div class="ldr"></div>
    </div>
</div>

<!-- Wrap -->
<div id="wrap">

    <!- Подключение шапки сайта ->
    <?php require_once('template/main/header.php') ?>

    <!-- Баннер с акциями -->
    <section class="home-slider simple-head" data-stellar-background-ratio="0.5">

        <!-- Container Fluid -->
        <div class="container-fluid">
            <div class="position-center-center">

                <!-- Header Text -->
                <div class="col-lg-7 col-lg-offset-5"> <span class="price"><small>-30%</small></span>
                    <h1 class="extra-huge-text">На всю учебную литературу!</h1>
                    <div class="text-center"> <a href="shop.php" class="btn btn-round margin-top-40">Подробнее</a> </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Content -->
    <div id="content">

        <!-- Новинки литературы -->
        <section class="padding-top-100 padding-bottom-100">
            <div class="container">

                <!-- Main Heading -->
                <div class="heading text-center">
                    <h4>Новинки литературы</h4>
                </div>
            </div>

            <!-- New Arrival -->
            <div class="arrival-block">

                <!-- Item -->
                <?php foreach ($new_book as $new_books) : ?>
                <div class="item">
                    <!-- Images -->
                    <img class="img" src="images/new_book/<?= $new_books['image_1'] ?>" alt="">
                    <!-- Sale Tags -->
                    <?php if ($new_books['discount'] == 0) : ?>
                    <?php else : ?>
                        <div class="on-sale"><?= $new_books['discount'] ?>%<span> OFF</span> </div>
                    <?php endif; ?>
                    <!-- Overlay  -->
                    <div class="overlay">
                        <!-- Price -->
                        <?php $price = $new_books['price'] - ($new_books['price'] * ($new_books['discount'] / 100)) ?>
                        <span class="price"><small><?= floor ($price); ?>₽</small></span>
                        <div class="position-center-center">
                            <div class="inn"> <a href="cart.php" data-toggle="tooltip" data-placement="top" title="Корзина"><i class="icon-basket"></i></a> <a href="#." data-toggle="tooltip" data-placement="top" title="Избранное"><i class="icon-heart"></i></a></div>
                        </div>
                    </div>
                    <!-- Item Name -->
                    <div class="item-name"> <a href="product.php"><?= $new_books['name'] ?></a>
                        <p><?= $new_books['author'] ?></p>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </section>

        <!-- Популярное -->
        <section class="padding-top-50 padding-bottom-150">
            <div class="container">

                <!-- Main Heading -->
                <div class="heading text-center">
                    <h4>Популярное</h4>
                    </div>

                <!-- Popular Item Slide -->
                <div class="papular-block block-slide">

                    <!-- Item -->
                    <?php foreach ($best_book as $best_books) : ?>
                    <div class="item">
                        <!-- Item img -->
                        <div class="item"> <img class="img" src="images/best_book/<?= $best_books['image_2'] ?>" alt="" ></div>
                        <!-- Sale Tags -->
                        <?php if ($best_books['discount'] == 0) : ?>
                        <?php else : ?>
                        <div class="on-sale"><?= $best_books['discount'] ?>%<span> OFF</span> </div>
                        <?php endif; ?>
                        <!-- Item Name -->
                        <div class="item-name"> <a href="product.php"><?= $best_books['name'] ?></a>
                            <p><?= $best_books['author'] ?></p>
                        </div>
                        <!-- Price -->
                        <?php $price = $best_books['price'] - ($best_books['price'] * ($best_books['discount'] / 100)) ?>
                        <span class="price"><small><?= floor ($price);  ?> ₽</small></span>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </section>

        <!-- Горящие товары -->
        <section class="padding-top-100 padding-bottom-100">
            <div class="container">

                <!-- Main Heading -->
                <div class="heading text-center">
                    <h4>Горящие товары</h4>
                </div>
            </div>

            <!--  -->
            <div class="arrival-block">

                <!-- Item -->
                <?php foreach ($burn_book as $burn_books) : ?>
                    <div class="item">
                        <!-- Images -->
                        <img class="img" src="images/new_book/<?= $burn_books['image_1'] ?>" alt="">
                        <!-- Sale Tags -->
                        <div class="on-sale"><?= $burn_books['discount'] ?>%<span> OFF</span> </div>
                        <!-- Overlay  -->
                        <div class="overlay">
                            <!-- Price -->
                            <?php $price = $burn_books['price'] - ($burn_books['price'] * ($burn_books['discount'] / 100)) ?>
                            <span class="price"><small><?= floor ($price);  ?>₽</small></span>
                            <div class="position-center-center">
                                <div class="inn"> <a href="cart.php" data-toggle="tooltip" data-placement="top" title="Корзина"><i class="icon-basket"></i></a> <a href="#." data-toggle="tooltip" data-placement="top" title="Избранное"><i class="icon-heart"></i></a></div>
                            </div>
                        </div>
                        <!-- Item Name -->
                        <div class="item-name"> <a href="product.php"><?= $burn_books['name'] ?></a>
                            <p><?= $burn_books['author'] ?></p>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </section>

        <!-- Скоро в продаже -->
        <section class="padding-top-50 padding-bottom-150">
            <div class="container">

                <!-- Main Heading -->
                <div class="heading text-center">
                    <h4>Скоро в продаже</h4>
                </div>

                <!-- Popular Item Slide -->
                <div class="papular-block block-slide">

                    <!-- Item -->
                    <?php foreach ($soon_book as $soon_books) : ?>
                        <div class="item">
                            <!-- Item img -->
                            <div class="item"> <img class="img" src="images/best_book/<?= $soon_books['image_2'] ?>" alt="" ></div>
                            <!-- Sale Tags -->
                            <?php if ($soon_books['discount'] == 0) : ?>
                            <?php else : ?>
                                <div class="on-sale"><?= $soon_books['discount'] ?>%<span> OFF</span> </div>
                            <?php endif; ?>
                            <!-- Item Name -->
                            <div class="item-name"> <a href="product.php"><?= $soon_books['name'] ?></a>
                                <p><?= $soon_books['author'] ?></p>
                            </div>
                            <!-- Price -->
                            <?php $price = $soon_books['price'] - ($soon_books['price'] * ($soon_books['discount'] / 100)) ?>
                            <span class="price"><small><?= floor ($price);  ?> ₽</small></span>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </section>

        <!- Подключение новостной рассылки сайта ->
        <?php require_once('template/main/newsletter.php') ?>
    </div>

    <!- Подключение подвала сайта ->
    <?php require_once('template/main/footer.php') ?>

    <!--======= RIGHTS =========-->

</div>
<script src="js/jquery-1.11.3.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/own-menu.js"></script>
<script src="js/jquery.lighter.js"></script>
<script src="js/owl.carousel.min.js"></script>

<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
<script type="text/javascript" src="rs-plugin/js/jquery.tp.t.min.js"></script>
<script type="text/javascript" src="rs-plugin/js/jquery.tp.min.js"></script>
<script src="js/main.js"></script>
<script src="js/main.js"></script>
</body>
</html>
