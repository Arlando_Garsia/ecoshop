<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="M_Adnan">
    <title>ECOSHOP - Multipurpose eCommerce HTML5 Template</title>

    <!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="rs-plugin/css/settings.css" media="screen" />

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="css/ionicons.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    <!-- JavaScripts -->
    <script src="js/modernizr.js"></script>

    <!-- Online Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Playfair+Display:400,700,900' rel='stylesheet' type='text/css'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>

<!-- LOADER -->
<div id="loader">
    <div class="position-center-center">
        <div class="ldr"></div>
    </div>
</div>

<!-- Wrap -->
<div id="wrap">

    <!- Подключение шапки сайта ->
    <?php require_once('template/main/header.php') ?>

    <!--======= SUB BANNER =========-->
    <section class="sub-bnr" data-stellar-background-ratio="0.5">
        <div class="position-center-center">
            <div class="container">
                <h4>ОПЛАТА</h4>
                <ol class="breadcrumb">
                    <li><a href="/">Главная</a></li>
                    <li><a href="shop.php">Каталог</a></li>
                    <li class="active">Оплата</li>
                </ol>
            </div>
        </div>
    </section>

    <!-- Content -->
    <div id="content">

        <!--======= PAGES INNER =========-->
        <section class="chart-page padding-top-100 padding-bottom-100">
            <div class="container">

                <!-- Payments Steps -->
                <div class="shopping-cart">

                    <!-- SHOPPING INFORMATION -->
                    <div class="cart-ship-info">
                        <div class="row">

                            <!-- ESTIMATE SHIPPING & TAX -->
                            <div class="col-sm-7">
                                <h6>BILLING DETAILS</h6>
                                <form>
                                    <ul class="row">

                                        <!-- Name -->
                                        <li class="col-md-6">
                                            <label> *FIRST NAME
                                                <input type="text" name="first-name" value="" placeholder="">
                                            </label>
                                        </li>
                                        <!-- LAST NAME -->
                                        <li class="col-md-6">
                                            <label> *LAST NAME
                                                <input type="text" name="last-name" value="" placeholder="">
                                            </label>
                                        </li>
                                        <li class="col-md-6">
                                            <!-- COMPANY NAME -->
                                            <label>COMPANY NAME
                                                <input type="text" name="company" value="" placeholder="">
                                            </label>
                                        </li>
                                        <li class="col-md-6">
                                            <!-- ADDRESS -->
                                            <label>*ADDRESS
                                                <input type="text" name="address" value="" placeholder="">
                                            </label>
                                        </li>
                                        <!-- TOWN/CITY -->
                                        <li class="col-md-6">
                                            <label>*TOWN/CITY
                                                <input type="text" name="town" value="" placeholder="">
                                            </label>
                                        </li>

                                        <!-- COUNTRY -->
                                        <li class="col-md-6">
                                            <label> COUNTRY
                                                <input type="text" name="contry-state" value="" placeholder="">
                                            </label>
                                        </li>

                                        <!-- EMAIL ADDRESS -->
                                        <li class="col-md-6">
                                            <label> *EMAIL ADDRESS
                                                <input type="text" name="contry-state" value="" placeholder="">
                                            </label>
                                        </li>
                                        <!-- PHONE -->
                                        <li class="col-md-6">
                                            <label> *PHONE
                                                <input type="text" name="postal-code" value="" placeholder="">
                                            </label>
                                        </li>

                                        <!-- PHONE -->
                                        <li class="col-md-6">
                                            <button type="submit" class="btn">continue</button>
                                        </li>

                                        <!-- CREATE AN ACCOUNT -->
                                        <li class="col-md-6">
                                            <div class="checkbox margin-0 margin-top-20">
                                                <input id="checkbox1" class="styled" type="checkbox">
                                                <label for="checkbox1"> Ship to a different address </label>
                                            </div>
                                        </li>
                                    </ul>
                                </form>

                                <!-- SHIPPING info -->
                                <h6 class="margin-top-50">SHIPPING info</h6>
                                <form>
                                    <ul class="row">

                                        <!-- Name -->
                                        <li class="col-md-6">
                                            <label> *FIRST NAME
                                                <input type="text" name="first-name" value="" placeholder="">
                                            </label>
                                        </li>
                                        <!-- LAST NAME -->
                                        <li class="col-md-6">
                                            <label> *LAST NAME
                                                <input type="text" name="last-name" value="" placeholder="">
                                            </label>
                                        </li>
                                        <li class="col-md-6">
                                            <!-- COMPANY NAME -->
                                            <label>COMPANY NAME
                                                <input type="text" name="company" value="" placeholder="">
                                            </label>
                                        </li>
                                        <li class="col-md-6">
                                            <!-- ADDRESS -->
                                            <label>*ADDRESS
                                                <input type="text" name="address" value="" placeholder="">
                                            </label>
                                        </li>
                                        <!-- TOWN/CITY -->
                                        <li class="col-md-6">
                                            <label>*TOWN/CITY
                                                <input type="text" name="town" value="" placeholder="">
                                            </label>
                                        </li>

                                        <!-- COUNTRY -->
                                        <li class="col-md-6">
                                            <label> COUNTRY
                                                <input type="text" name="contry-state" value="" placeholder="">
                                            </label>
                                        </li>

                                        <!-- EMAIL ADDRESS -->
                                        <li class="col-md-6">
                                            <label> *EMAIL ADDRESS
                                                <input type="text" name="contry-state" value="" placeholder="">
                                            </label>
                                        </li>
                                        <!-- PHONE -->
                                        <li class="col-md-6">
                                            <label> *PHONE
                                                <input type="text" name="postal-code" value="" placeholder="">
                                            </label>
                                        </li>

                                        <!-- PHONE -->
                                        <li class="col-md-6">
                                            <button type="submit" class="btn">SUBMIT</button>
                                        </li>
                                    </ul>
                                </form>
                            </div>

                            <!-- SUB TOTAL -->
                            <div class="col-sm-5">
                                <h6>YOUR ORDER</h6>
                                <div class="order-place">
                                    <div class="order-detail">
                                        <p>WOOD CHAIR <span>$598 </span></p>
                                        <p>STOOL <span>$199 </span></p>
                                        <p>WOOD SPOON <span> $139</span></p>

                                        <!-- SUB TOTAL -->
                                        <p class="all-total">TOTAL COST <span> $998</span></p>
                                    </div>
                                    <div class="pay-meth">
                                        <ul>
                                            <li>
                                                <div class="radio">
                                                    <input type="radio" name="radio1" id="radio1" value="option1" checked>
                                                    <label for="radio1"> DIRECT BANK TRANSFER </label>
                                                </div>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam erat turpis, pellentesque non leo eget, pulvinar pretium arcu. Mauris porta elit non.</p>
                                            </li>
                                            <li>
                                                <div class="radio">
                                                    <input type="radio" name="radio1" id="radio2" value="option2">
                                                    <label for="radio2"> CASH ON DELIVERY</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="radio">
                                                    <input type="radio" name="radio1" id="radio3" value="option3">
                                                    <label for="radio3"> CHEQUE PAYMENT </label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="radio">
                                                    <input type="radio" name="radio1" id="radio4" value="option4">
                                                    <label for="radio4"> PAYPAL </label>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="checkbox">
                                                    <input id="checkbox3-4" class="styled" type="checkbox">
                                                    <label for="checkbox3-4"> I’VE READ AND ACCEPT THE <span class="color"> TERMS & CONDITIONS </span> </label>
                                                </div>
                                            </li>
                                        </ul>
                                        <a href="#." class="btn  btn-dark pull-right margin-top-30">PLACE ORDER</a> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!- Подключение новостной рассылки сайта ->
        <?php require_once('template/main/newsletter.php') ?>
    </div>

    <!- Подключение подвала сайта ->
    <?php require_once('template/main/footer.php') ?>

    <!--======= RIGHTS =========-->

</div>
<script src="js/jquery-1.11.3.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/own-menu.js"></script>
<script src="js/jquery.lighter.js"></script>
<script src="js/owl.carousel.min.js"></script>

<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
<script type="text/javascript" src="rs-plugin/js/jquery.tp.t.min.js"></script>
<script type="text/javascript" src="rs-plugin/js/jquery.tp.min.js"></script>
<script src="js/main.js"></script>
<script src="js/main.js"></script>
</body>
</html>
